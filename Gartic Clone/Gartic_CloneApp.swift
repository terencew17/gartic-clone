//
//  Gartic_CloneApp.swift
//  Gartic Clone
//
//  Created by Terence Wong on 9/15/23.
//

import SwiftUI

@main
struct Gartic_CloneApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
